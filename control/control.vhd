library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity control is
    port (
                 clk, mclk : in std_logic;
                    enable : in std_logic;
          statusC, statusZ : in std_logic;
                      INST : in std_logic_vector(31 downto 0);
              A_Mux, B_Mux : out std_logic;
          IM_MUX1, REG_Mux : out std_logic;
         IM_MUX2, DATA_Mux : out std_logic_vector(1 downto 0);
                    ALU_op : out std_logic_vector(2 downto 0);
             inc_PC, ld_PC : out std_logic;
                    clr_IR : out std_logic;
                     ld_IR : out std_logic;
clr_A, clr_B, clr_C, clr_Z : out std_logic;
    ld_A, ld_B, ld_C, ld_Z : out std_logic;
                         T : out std_logic_vector(2 downto 0);
                   wen, en : out std_logic);
end control;

architecture description of control is
    type STATETYPE is (state_0, state_1, state_2);
    signal present_state : STATETYPE;

    subtype nibble is std_logic_vector(3 downto 0);
    subtype halfWord is std_logic_vector(15 downto 0);

    signal firstNibble : nibble;
    signal secondNibble : nibble;
    signal lowerHalfWord : halfWord;
begin
    firstNibble <= INST(31 downto 28);
    secondNibble <= INST(27 downto 24);
    lowerHalfWord <= INST(15 downto 0);
    
    process (clk, enable) -- STATE MACHINE
    begin
        if enable = '1' then
            if falling_edge(clk) then
                case present_state is
                    when state_0 => T <= "010"; present_state <= state_1;
                    when state_1 => T <= "100"; present_state <= state_2;
                    when others => T <= "001"; present_state <= state_0;
                end case;
            end if;
        else
            T <= "001";
        end if;
    end process;

    process (mclk, clk, INST) -- MEMORY SIGNAL GENERATOR
    begin
        if (falling_edge(mclk)) then
            if (present_state = state_1 and clk = '0') then
                case firstNibble is
                    when "0010" => --STA
                        en <= '1'; wen <= '1';
                    when "0011" => --STB
                        en <= '1'; wen <= '1';
                    when "1001" => --LDA
                        en <= '1'; wen <= '0';
                    when "1010" => --LDB
                        en <= '1'; wen <= '0';
                    when others =>
                        en <= '0'; wen <= '0';
                end case;
            elsif (present_state = state_2 and clk = '1') then
                en <= '0'; wen <= '0';
            end if;
        end if;
    end process;

    process (present_state, INST, statusC, statusZ, enable) -- OPERATION DECODER
    begin
        if enable = '1' then
            case present_state is
                when state_0 => --T0 stage: Instruction Register (IR) <= Instruction Memory[PC]
                    clr_IR <= '0'; ld_IR <= '1';
                    ld_PC <= '0'; inc_PC <= '0';
                    clr_A <= '0'; ld_A <= '0';
                    clr_B <= '0'; ld_B <= '0';
                    clr_C <= '0'; ld_C <= '0';
                    clr_Z <= '0'; ld_Z <= '0';
                    DATA_Mux <= "00";
                
                when state_1 => --T1 stage: Increment Program Counter (PC)
                    clr_IR <= '0'; ld_IR <= '0';
                    ld_PC <= '1'; inc_PC <= '1';
                    clr_A <= '0'; ld_A <= '0';
                    clr_B <= '0'; ld_B <= '0';
                    clr_C <= '0'; ld_C <= '0';
                    clr_Z <= '0'; ld_Z <= '0';

                when others => --T2 stage: Execute Instruction
                    case firstNibble is
                        when "0000" => --LDAI
                            clr_IR <= '0'; ld_IR <= '0';
                            ld_PC <= '0'; inc_PC <= '0';
                            clr_A <= '0'; ld_A <= '1';
                            clr_B <= '0'; ld_B <= '0';
                            clr_C <= '0'; ld_C <= '0';
                            clr_Z <= '0'; ld_Z <= '0';
                            A_Mux <= '1';

                        when "0001" => --LDBI
                            clr_IR <= '0'; ld_IR <= '0';
                            ld_PC <= '0'; inc_PC <= '0';
                            clr_A <= '0'; ld_A <= '0';
                            clr_B <= '0'; ld_B <= '1';
                            clr_C <= '0'; ld_C <= '0';
                            clr_Z <= '0'; ld_Z <= '0';
                            B_Mux <= '1';

                        when "0010" => --STA
                            clr_IR <= '0'; ld_IR <= '0';
                            ld_PC <= '0'; inc_PC <= '0';
                            clr_A <= '0'; ld_A <= '0';
                            clr_B <= '0'; ld_B <= '0';
                            clr_C <= '0'; ld_C <= '0';
                            clr_Z <= '0'; ld_Z <= '0';
                            REG_Mux <= '0';

                        when "0011" => --STB
                            clr_IR <= '0'; ld_IR <= '0';
                            ld_PC <= '0'; inc_PC <= '0';
                            clr_A <= '0'; ld_A <= '0';
                            clr_B <= '0'; ld_B <= '0';
                            clr_C <= '0'; ld_C <= '0';
                            clr_Z <= '0'; ld_Z <= '0';
                            REG_Mux <= '1';

                        when "1001" => --LDA
                            clr_IR <= '0'; ld_IR <= '0';
                            ld_PC <= '0'; inc_PC <= '0';
                            clr_A <= '0'; ld_A <= '1';
                            clr_B <= '0'; ld_B <= '0';
                            clr_C <= '0'; ld_C <= '0';
                            clr_Z <= '0'; ld_Z <= '0';
                            A_Mux <= '0';
                            DATA_Mux <= "01";

                        when "1010" => --LDB
                            clr_IR <= '0'; ld_IR <= '0';
                            ld_PC <= '0'; inc_PC <= '0';
                            clr_A <= '0'; ld_A <= '0';
                            clr_B <= '0'; ld_B <= '1';
                            clr_C <= '0'; ld_C <= '0';
                            clr_Z <= '0'; ld_Z <= '0';
                            B_Mux <= '0';
                            DATA_Mux <= "01";

                        when "0100" => --LUI
                            clr_IR <= '0'; ld_IR <= '0';
                            ld_PC <= '0'; inc_PC <= '0';
                            clr_A <= '0'; ld_A <= '1';
                            clr_B <= '1'; ld_B <= '0';
                            clr_C <= '0'; ld_C <= '0';
                            clr_Z <= '0'; ld_Z <= '0';
                            ALU_op <= "001";
                            A_Mux <= '0';
                            DATA_Mux <= "10";
                            IM_MUX1 <= '1';

                        when "0101" => --JMP
                            clr_IR <= '0'; ld_IR <= '0';
                            ld_PC <= '1'; inc_PC <= '0';
                            clr_A <= '0'; ld_A <= '0';
                            clr_B <= '0'; ld_B <= '0';
                            clr_C <= '0'; ld_C <= '0';
                            clr_Z <= '0'; ld_Z <= '0';

                        when others =>
                            case secondNibble is
                                when "0000" => --ADD
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "010";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                                    IM_MUX2 <= "00";

                                when "0001" => --ADDI
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "010";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                                    IM_MUX2 <= "01";

                                when "0010" => --SUB
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "110";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                                    IM_MUX2 <= "00";

                                when "0011" => --INCA
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "010";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                                    IM_MUX2 <= "10";

                                when "0100" => --ROL
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "100";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';

                                when "0101" => --CLRA
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '1'; ld_A <= '0';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '0';
                                    clr_Z <= '0'; ld_Z <= '0';

                                when "0110" => --CLRB
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '0';
                                    clr_B <= '1'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '0';
                                    clr_Z <= '0'; ld_Z <= '0';

                                when "0111" => --CLRC
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '0';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '1'; ld_C <= '0';
                                    clr_Z <= '0'; ld_Z <= '0';

                                when "1000" => --CLRZ
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '0';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '0';
                                    clr_Z <= '1'; ld_Z <= '0';

                                when "1001" => --ANDI
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "000";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                                    IM_MUX2 <= "01";

                                when "1011" => --AND
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "000";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                                    IM_MUX2 <= "00";

                                when "1101" => --ORI
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "001";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                                    IM_MUX2 <= "01";

                                when "1110" => --DECA
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "110";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                                    IM_MUX2 <= "10";

                                when others => --ROR
                                    clr_IR <= '0'; ld_IR <= '0';
                                    ld_PC <= '0'; inc_PC <= '0';
                                    clr_A <= '0'; ld_A <= '1';
                                    clr_B <= '0'; ld_B <= '0';
                                    clr_C <= '0'; ld_C <= '1';
                                    clr_Z <= '0'; ld_Z <= '1';
                                    ALU_op <= "101";
                                    A_mux <= '0';
                                    DATA_Mux <= "10";
                                    IM_MUX1 <= '0';
                            end case;
                    end case;
            end case;
        end if;
    end process;
end description;