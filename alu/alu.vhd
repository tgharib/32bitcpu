library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is
	generic (NUM_OF_BITS : integer := 32);
	port (a, b : in std_logic_vector(NUM_OF_BITS - 1 downto 0);
           op : in std_logic_vector(2 downto 0);
       result : buffer std_logic_vector(NUM_OF_BITS - 1 downto 0);
    cout,zero : out std_logic);
end alu;

architecture description of alu is
	component n_bit_adder
		generic (NUM_OF_BITS : integer := 32);
		port (Cin : in std_logic;
			  a, b : in std_logic_vector(NUM_OF_BITS - 1 downto 0);
			     S : out std_logic_vector(NUM_OF_BITS - 1 downto 0);
			  Cout : out std_logic);
	end component;
	signal sum : std_logic_vector(NUM_OF_BITS - 1 downto 0);
	signal diff : std_logic_vector(NUM_OF_BITS - 1 downto 0);
	signal coutsum : std_logic;
	signal coutdiff : std_logic;
begin
	summer : n_bit_adder port map(op(2), a, b, sum, coutsum);
	differ : n_bit_adder port map(op(2), a, not b, diff, coutdiff);
	process (a, b, op)
	begin
		case op is
			when "000" => result <= a and b;
			when "001" => result <= a or b;
			when "010" =>
				result <= sum;
				cout <= coutsum;
			when "110" =>
				result <= diff;
				cout <= coutdiff;
			when "100" => result <= std_logic_vector(unsigned(a) sll 1);
			when "101" => result <= std_logic_vector(unsigned(a) srl 1);
			--when "010" => result <= std_logic_vector(unsigned(a) + unsigned(b));
			--when "110" => result <= std_logic_vector(unsigned(a) - unsigned(b));
			when others => result <= a;
		end case;
		if result = x"00000000" then
			zero <= '1';
		else
			zero <= '0';
		end if;
	end process;
end description;