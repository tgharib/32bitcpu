# TEST ALL INSTRUCTIONS THAT INVOLVE PC OR MEMORY
# The control signals diagram doesn't support TSTC/TSTZ/BEQ/BNE so it is impossible to implement/test TSTC/TSTZ/BEQ/BNE. For TSTC, INC_PC does not connect to register C in any way. For TSTZ, INC_PC does not connect to register Z in any way. For BEQ/BNE, ALU output does not connect to LD_PC in any way. Control signals for TSTC/TSTZ/BEQ/BNE are not generated.
# I could've tested JMP because it does not rely on LD_PC or INC_PC. I tested STA/LDB (but not STB/LDA because waste of time).
01000000000000001111111111111111 LUI 65535 # A should be 4294901760
00100000000000000000000000000000 STA 0     # MEM[0] should be 4294901760
10100000000000000000000000000000 LDB 0     # B should be 4294901760
00000000000000000000000000000101 LDAI 5    # A should be 5
01010000000000000000001111011010 JMP 986   # PC should be 986
00110000000000000000000000000100 STB 4     # MEM[4] should be 4294901760
10010000000000000000000000000100 LDA 4     # A should be 4294901760