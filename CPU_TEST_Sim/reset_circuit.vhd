library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reset_circuit is
  port (
    Reset : in std_logic;
    Clk : in std_logic;
    Enable_PD : out std_logic;
    Clr_PC : out std_logic
  );
end reset_circuit;

architecture description of reset_circuit is begin
  process (Reset, Clk)
    variable countRisingEdge : integer := 0;
  begin
    if Reset = '1' then
      Enable_PD <= '0';
      Clr_PC <= '1';
      countRisingEdge := 0;
    else
      if rising_edge(Clk) then
        countRisingEdge := countRisingEdge + 1;
      end if;
      
      if countRisingEdge >= 4 then --passed through 4 rising edges since reset so 4 clock cycles
        Enable_PD <= '1';
        Clr_PC <= '0';
      end if;
    end if;
  end process;
end description;
