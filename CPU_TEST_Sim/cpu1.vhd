library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cpu1 is
    port (
                -- Input ports
                clk : in std_logic; --DONE
            mem_clk : in std_logic;
                rst : in std_logic; --DONE
             dataIn : in std_logic_vector(31 downto 0);

             -- Output ports
            dataOut : out std_logic_vector(31 downto 0);
            addrOut : out std_logic_vector(31 downto 0);
                wEn : out std_logic;

                -- Debug data
       dOutA, dOutB : out std_logic_vector(31 downto 0);
       dOutC, dOutZ : out std_logic;
             dOutIR : out std_logic_vector(31 downto 0);
             dOutPC : out std_logic_vector(31 downto 0);
               outT : out std_logic_vector(2 downto 0);
    wen_mem, en_mem : out std_logic);
end cpu1;

architecture description of cpu1 is
    component datapath
        port(       Clk, mClk : in std_logic;
                      WEN, EN : in std_logic;

                  Clr_A, Ld_A : in std_logic;
                  Clr_B, Ld_B : in std_logic;
                  Clr_C, Ld_C : in std_logic;
                  Clr_Z, Ld_Z : in std_logic;
                Clr_PC, Ld_PC : in std_logic;
                Clr_IR, Ld_IR : in std_logic;

                        Out_A : buffer std_logic_vector(31 downto 0);
                        Out_B : buffer std_logic_vector(31 downto 0);
                        Out_C : out std_logic;
                        Out_Z : out std_logic;
                       Out_PC : buffer std_logic_vector(31 downto 0);
                       Out_IR : buffer std_logic_vector(31 downto 0);

                       Inc_PC : in std_logic;

                     ADDR_OUT : out std_logic_vector(31 downto 0);
                      DATA_IN : in std_logic_vector(31 downto 0);
    DATA_OUT, MEM_OUT, MEM_IN : buffer std_logic_vector(31 downto 0);
                     MEM_ADDR : buffer std_logic_vector(7 downto 0);

                     DATA_Mux : in std_logic_vector(1 downto 0);
                      REG_Mux : in std_logic;
                 A_MUX, B_MUX : in std_logic;
                      IM_MUX1 : in std_logic;
                      IM_MUX2 : in std_logic_vector(1 downto 0);

                       ALU_Op : in std_logic_vector(2 downto 0));
    end component;

    component control
        port (       clk, mclk : in std_logic;
                        enable : in std_logic;
              statusC, statusZ : in std_logic;
                          INST : in std_logic_vector(31 downto 0);
                  A_Mux, B_Mux : out std_logic;
              IM_MUX1, REG_Mux : out std_logic;
             IM_MUX2, DATA_Mux : out std_logic_vector(1 downto 0);
                        ALU_op : out std_logic_vector(2 downto 0);
                 inc_PC, ld_PC : out std_logic;
                        clr_IR : out std_logic;
                         ld_IR : out std_logic;
    clr_A, clr_B, clr_C, clr_Z : out std_logic;
        ld_A, ld_B, ld_C, ld_Z : out std_logic;
                             T : out std_logic_vector(2 downto 0);
                       wen, en : out std_logic);
    end component;
    
    component reset_circuit
        port (Reset : in std_logic;
                Clk : in std_logic;
          Enable_PD : out std_logic;
             Clr_PC : out std_logic);
    end component;
    
    -- Internal signals
    signal INTERNAL_ENABLE_PD : std_logic;
    signal INTERNAL_CLR_PC : std_logic; --TODO
begin
    datapathInstance : datapath port map();
    controlInstance : control port map(clk, mem_clk, INTERNAL_ENABLE_PD, '0', '0', dataIn); --TODO
    resetCircuitInstance : reset_circuit port map(rst, clk, INTERNAL_ENABLE_PD, INTERNAL_CLR_PC);
end description;