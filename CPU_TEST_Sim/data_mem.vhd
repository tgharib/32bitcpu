library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity data_mem is
  port(clk : in std_logic;
    addr : in unsigned(7 downto 0);
   data_in : in std_logic_vector(31 downto 0);
     wen : in std_logic;
    en : in std_logic;
  data_out : out std_logic_vector(31 downto 0));
end data_mem;

architecture Description of data_mem is
begin
  process(clk)
    subtype Word is std_logic_vector(31 downto 0);
    type Mem is array (0 to 2**8-1) of Word;
    variable Memory : Mem := (others => Word'(others => 'U'));
  begin
    if en = '1' then
      if falling_edge(clk) then
        if wen = '0' then --if reading from memory
          data_out <= Memory(to_integer(addr));
        elsif wen = '1' then --if writing to memory
          Memory(to_integer(addr)) := data_in;
          data_out <= x"00000000";
        end if;
      end if;
    else
      data_out <= x"00000000";
    end if;
  end process;
end Description;
