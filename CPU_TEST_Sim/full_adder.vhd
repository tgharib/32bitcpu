library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity full_adder is
	port (x, y, z : in std_logic;
		Sum, Carry : out std_logic);
end full_adder;

architecture gate_level of full_adder is
begin
	Sum <= x xor y xor z;
	Carry <= (x and y) or (z and x) or (z and y);
end gate_level;