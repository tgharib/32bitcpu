library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity datapath is
    port(       Clk, mClk : in std_logic; -- CPU + memory clock
                  WEN, EN : in std_logic; -- Memory read/write signals

                -- Register control signals (CLR and LD).
              Clr_A, Ld_A : in std_logic;
              Clr_B, Ld_B : in std_logic;
              Clr_C, Ld_C : in std_logic;
              Clr_Z, Ld_Z : in std_logic;
            Clr_PC, Ld_PC : in std_logic;
            Clr_IR, Ld_IR : in std_logic;

                -- Register outputs. Some are needed for feedback to control unit. Others are pulled out for testing.
                    Out_A : buffer std_logic_vector(31 downto 0);
                    Out_B : buffer std_logic_vector(31 downto 0);
                    Out_C : out std_logic;
                    Out_Z : out std_logic;
                   Out_PC : buffer std_logic_vector(31 downto 0);
                   Out_IR : buffer std_logic_vector(31 downto 0);

                -- Special inputs to PC.
                   Inc_PC : in std_logic;

                -- Address and data bus signals for debugging.
                 ADDR_OUT : out std_logic_vector(31 downto 0);
                  DATA_IN : in std_logic_vector(31 downto 0);
DATA_OUT, MEM_OUT, MEM_IN : buffer std_logic_vector(31 downto 0);
                 MEM_ADDR : buffer std_logic_vector(7 downto 0);

                -- Various MUX controls.
                 DATA_Mux : in std_logic_vector(1 downto 0);
                  REG_Mux : in std_logic;
             A_MUX, B_MUX : in std_logic;
                  IM_MUX1 : in std_logic;
                  IM_MUX2 : in std_logic_vector(1 downto 0);

                -- ALU operations.
                   ALU_Op : in std_logic_vector(2 downto 0));
end datapath;

architecture description of datapath is
    -- Component instantiations
    component register32
        port(d : IN std_logic_vector(31 downto 0);
            ld : IN std_logic;
           clr : IN std_logic;
           clk : IN std_logic;
             Q : OUT std_logic_vector(31 downto 0));
    end component;

    component register1
        port(d : IN std_logic;
            ld : IN std_logic;
           clr : IN std_logic;
           clk : IN std_logic;
             Q : OUT std_logic);
    end component;

    component pc
        port(clr : IN std_logic;
             clk : IN std_logic;
              ld : IN std_logic;
             inc : IN std_logic;
               d : IN std_logic_vector(31 downto 0);
               q : BUFFER std_logic_vector(31 downto 0));
    end component;

    component data_mem
        port(clk : in std_logic;
            addr : in unsigned(7 downto 0);
         data_in : in std_logic_vector(31 downto 0);
             wen : in std_logic;
              en : in std_logic;
        data_out : out std_logic_vector(31 downto 0));
    end component;

    component alu
        generic (NUM_OF_BITS : integer := 32);
        port (a, b : in std_logic_vector(NUM_OF_BITS - 1 downto 0);
                op : in std_logic_vector(2 downto 0);
            result : buffer std_logic_vector(NUM_OF_BITS - 1 downto 0);
         cout,zero : out std_logic);
    end component;

    -- Internal signals
    signal INPUT_TO_REGA : std_logic_vector(31 downto 0);
    signal INPUT_TO_REGB : std_logic_vector(31 downto 0);
    signal INPUT1_TO_ALU : std_logic_vector(31 downto 0);
    signal INPUT2_TO_ALU : std_logic_vector(31 downto 0);
    signal INPUT_TO_PC : std_logic_vector(31 downto 0);
    signal INTERNAL_DATA_BUS : std_logic_vector(31 downto 0);
    signal INTERNAL_CARRY : std_logic;
    signal INTERNAL_ZERO : std_logic;
    signal INTERNAL_ALU_OUTPUT : std_logic_vector(31 downto 0);
begin
    regA : register32 port map(INPUT_TO_REGA, LD_A, CLR_A, Clk, Out_A);
    regB : register32 port map(INPUT_TO_REGB, LD_B, CLR_B, Clk, Out_B);
    regC : register1 port map(INTERNAL_CARRY, LD_C, CLR_C, Clk, Out_C);
    regZ : register1 port map(INTERNAL_ZERO, LD_Z, CLR_Z, Clk, Out_Z);
    regPC : pc port map(CLR_PC, Clk, LD_PC, INC_PC, INPUT_TO_PC, OUT_PC);
    regIR : register32 port map(INTERNAL_DATA_BUS, LD_IR, CLR_IR, Clk, OUT_IR);
    instanceALU : alu port map(INPUT1_TO_ALU, INPUT2_TO_ALU, Alu_Op, INTERNAL_ALU_OUTPUT, INTERNAL_CARRY, INTERNAL_ZERO);
    memory : data_mem port map(mClk, unsigned(MEM_ADDR), MEM_IN, WEN, EN, MEM_OUT);
    
    ADDR_OUT <= OUT_PC;
    MEM_ADDR <= OUT_IR(7 downto 0);
    DATA_OUT <= INTERNAL_DATA_BUS;
    
    INPUT_TO_PC(31 downto  16) <= (others => '0');
    INPUT_TO_PC(15 downto  0) <= OUT_IR(15 downto 0);

    with A_MUX select
        INPUT_TO_REGA(31 downto 16) <= INTERNAL_DATA_BUS(31 downto 16) when '0',
                                       (others => '0') when others;
    with A_MUX select
        INPUT_TO_REGA(15 downto 0) <= INTERNAL_DATA_BUS(15 downto 0) when '0',
                                      OUT_IR(15 downto 0) when others;

    with B_MUX select
        INPUT_TO_REGB(31 downto 16) <= INTERNAL_DATA_BUS(31 downto 16) when '0',
                                       (others => '0') when others;
    with B_MUX select
        INPUT_TO_REGB(15 downto 0) <= INTERNAL_DATA_BUS(15 downto 0) when '0',
                                      OUT_IR(15 downto 0) when others;

    with REG_MUX select
        MEM_IN <= OUT_A when '0',
                  OUT_B when '1';

    with IM_MUX1 select
        INPUT1_TO_ALU(31 downto 16) <= OUT_A(31 downto 16) when '0',
                                       OUT_IR(15 downto 0) when others;
    with IM_MUX1 select
        INPUT1_TO_ALU(15 downto 0) <= OUT_A(15 downto 0) when '0',
                                      (others => '0') when others;

    with IM_MUX2 select
        INPUT2_TO_ALU(31 downto 16) <= OUT_B(31 downto 16) when "00",
                                       (others => '0') when "01",
                                       (others => '0') when others;
    with IM_MUX2 select
        INPUT2_TO_ALU(15 downto 0) <= OUT_B(15 downto 0) when "00",
                                      OUT_IR(15 downto 0) when "01",
                                      "0000000000000001" when others;

    with DATA_MUX select
        INTERNAL_DATA_BUS <= DATA_IN when "00",
                             MEM_OUT when "01",
                             INTERNAL_ALU_OUTPUT when others;
end description;