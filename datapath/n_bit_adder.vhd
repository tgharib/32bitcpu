library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity n_bit_adder is
	generic (NUM_OF_BITS : integer := 32);
	port (Cin : in std_logic;
		  a, b : in std_logic_vector(NUM_OF_BITS - 1 downto 0);
		     S : out std_logic_vector(NUM_OF_BITS - 1 downto 0);
		  Cout : out std_logic);
end;

architecture ripple_n_arch of n_bit_adder is
	component full_adder
		port (x, y, z : in std_logic;
			Sum, Carry : out std_logic);
	end component;
	signal t : std_logic_vector(NUM_OF_BITS downto 0);
begin
	t(0) <= Cin;
	Cout <= t(NUM_OF_BITS);
	FA : for i in 0 to NUM_OF_BITS - 1 generate
		FA_i : full_adder port map(t(i), a(i), b(i), S(i), t(i + 1));
	end generate;
end;