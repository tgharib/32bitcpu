--8-Bit ALU test-fixture circuit.
--This circuit relies on the presence of the sevenSeg_8bit.vhd file in the same project.

library ieee;
use ieee.std_logic_1164.all;

entity lab3_b is
	port (
		--Input Switches
		SW : in std_logic_vector(17 downto 0);
		--Seven-Segment Display Outputs
		HEX0, HEX1 : out std_logic_vector(6 downto 0);
		HEX2, HEX3 : out std_logic_vector(6 downto 0);
		HEX4, HEX5 : out std_logic_vector(6 downto 0);
		HEX6, HEX7 : out std_logic_vector(6 downto 0);
		--Green Led Outputs
		LEDG : out std_logic_vector(8 downto 0);
		--Red Led Outputs
		LEDR : out std_logic_vector(17 downto 0)
	);
end lab3_b;

architecture behavior of lab3_b is
	--Use these signals to connect inputs to your ALU. A and B 
	--are the operand inputs and op is the Op-Code input.
	signal A_to_ALU : std_logic_vector(7 downto 0);
	signal B_to_ALU : std_logic_vector(7 downto 0);
	signal op_to_ALU : std_logic_vector(2 downto 0);

	--Use these signals to connect ALU outputs to the seven-segment displays
	--and leds. Result is the 8-bit output of the ALU. Zero and Cout should
	--be self-evident.
	signal result_from_ALU : std_logic_vector(7 downto 0);
	signal zero_from_ALU : std_logic;
	signal cout_from_ALU : std_logic;

	signal tmpInv : std_logic;

	component sevenSeg_8bit
		port (
			-- 8-bit signed number input.
			dataIn : in std_logic_vector(7 downto 0);
			-- 7-bit segment control signals.
			segVal1 : out std_logic_vector(6 downto 0);
			segVal2 : out std_logic_vector(6 downto 0);
			-- 1-bit sign control signal.
			sign : out std_logic
		);
	end component;

	--==========================================================
	-- This is where your ALU component declarations should go.
	--==========================================================
	component alu
		generic (NUM_OF_BITS : integer := 8);
		port (a, b : in std_logic_vector(NUM_OF_BITS - 1 downto 0);
				  op : in std_logic_vector(2 downto 0);
			 result : buffer std_logic_vector(NUM_OF_BITS - 1 downto 0);
		 cout,zero : out std_logic);
	end component;
begin
	--Signal routing from the input/output pins to the ALU and other utilities.
	A_to_ALU <= SW(17 downto 10);
	B_to_ALU <= SW(10 downto 3);
	op_to_ALU <= SW(2 downto 0);

	LEDG(0) <= zero_from_ALU;
	LEDG(1) <= cout_from_ALU;

	HEX2(6) <= not tmpInv;

	--Constants
	HEX2(5 downto 0) <= "111111";
	HEX3(6 downto 0) <= "1111111";

	sevSeg_driver_A : sevenSeg_8bit
	port map
	(
		dataIn => A_to_ALU,
		segVal1 => HEX6,
		segVal2 => HEX7,
		sign => LEDR(16)
	);

	sevSeg_driver_B : sevenSeg_8bit
	port map
	(
		dataIn => B_to_ALU,
		segVal1 => HEX4,
		segVal2 => HEX5,
		sign => LEDR(13)
	);

	sevSeg_driver_ALU : sevenSeg_8bit
	port map
	(
		dataIn => result_from_ALU,
		segVal1 => HEX0,
		segVal2 => HEX1,
		sign => tmpInv
	);

	--==============================================================================================
	--This is where the ALU must be connected. The signal names should be changed to match your ALU.
	--==============================================================================================

	the_ALU : alu
	port map
	(
		--Operand A
		a => A_to_ALU,
		--Operand B
		b => B_to_ALU,
		--Operation Code
		op => op_to_ALU,
		--ALU Result
		result => result_from_ALU,
		--Cout Signal
		cout => cout_from_ALU,
		--Zero Signal
		zero => zero_from_ALU
	);

end behavior;