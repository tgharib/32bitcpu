-- 7-Segment display decoder for 8-bit, 2's complement signed numbers.
-- Usage: connect the desired binary number to be displayed to dataIn and the two output vectors to two 7-segment displays.
-- NOTE: the decoder is built for active low displays, and segVal1(0) coresponds to led 0 in standard seven-segment displays.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;

entity sevenSeg_8bit is
	port (
		-- 8-bit signed number input.
		dataIn : in std_logic_vector(7 downto 0);
		-- 7-bit segment control signals.
		segVal1 : out std_logic_vector(6 downto 0);
		segVal2 : out std_logic_vector(6 downto 0);
		-- 1-bit sign control signal.
		sign : out std_logic
	);
end sevenSeg_8bit;
architecture behaviour of sevenSeg_8bit is
	signal numVal : std_logic_vector(6 downto 0);
	signal numVal2 : std_logic_vector(6 downto 0);
	signal numProcess : std_logic_vector(3 downto 0);
	signal numProcess2 : std_logic_vector(2 downto 0);
	signal signInternal : std_logic;
begin

	numVal <= dataIn(6 downto 0);
	signInternal <= dataIn(7);
	sign <= signInternal;

	-- Perform the appropriate conversion if the input is negative.
	negative_decode :
	process (signInternal)
	begin
		if (signInternal = '1') then
			numVal2 <= numVal - 1;
			numProcess <= not numVal2(3 downto 0);
			numProcess2 <= not numVal2(6 downto 4);
		else
			numProcess <= numVal(3 downto 0);
			numProcess2 <= numVal(6 downto 4);
		end if;
	end process negative_decode;

	-- Generate the first set of 7-segment control signal.
	sevenSeg_decode2 :
	process (numProcess2)
	begin
		case numProcess2 is
			when "000" => --0
				segVal2 <= "1000000";
			when "001" => --1
				segVal2 <= "1111001";
			when "010" => --2
				segVal2 <= "0100100";
			when "011" => --3
				segVal2 <= "0110000";
			when "100" => --4
				segVal2 <= "0011001";
			when "101" => --5
				segVal2 <= "0010010";
			when "110" => --6
				segVal2 <= "0000010";
			when "111" => --7
				segVal2 <= "1111000";
		end case;

	end process sevenSeg_decode2;

	-- Generate the second set of 7-segment control signal.
	sevenSeg_decode1 :
	process (numProcess)
	begin
		case numProcess is
			when "0000" => --0
				segVal1 <= "1000000";
			when "0001" => --1
				segVal1 <= "1111001";
			when "0010" => --2
				segVal1 <= "0100100";
			when "0011" => --3
				segVal1 <= "0110000";
			when "0100" => --4
				segVal1 <= "0011001";
			when "0101" => --5
				segVal1 <= "0010010";
			when "0110" => --6
				segVal1 <= "0000010";
			when "0111" => --7
				segVal1 <= "1111000";
			when "1000" => --8
				segVal1 <= "0000000";
			when "1001" => --9
				segVal1 <= "0011000";
			when "1010" => --A
				segVal1 <= "0001000";
			when "1011" => --B
				segVal1 <= "0000011";
			when "1100" => --C
				segVal1 <= "1000110";
			when "1101" => --D
				segVal1 <= "0100001";
			when "1110" => --E
				segVal1 <= "0000110";
			when "1111" => --F
				segVal1 <= "0001110";
		end case;

	end process sevenSeg_decode1;

end behaviour;